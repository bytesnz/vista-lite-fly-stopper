// Printed thichness height was 1.3 instead of 1.2
// Thickness of all walls
thickness = 1.2;
// Width
width = 13.1;
// Height of sides (excl top) should be 5 - thickness
height = 3.6; // 4.66
// Length from wall (excl top)
offWall = 4;
// Length of flat top
length = 12;
// Position of walls from from the top
space = 1.2;
bump = 0.15;
bumpLength = 3;

// Flat top
color("Green")
translate([
    offWall,
    0,
    height - 0.001
])
cube([
    length,
    width,
    thickness + 0.001
]);

// Bottom Sides
translate([
    0,
    thickness + space,
    0
])
rotate([90, 0, 0])
linear_extrude(thickness)
polygon([
    [offWall, 0],
    [0, offWall],
    [0, height + (0.5 * offWall)],
    [0.5 * offWall, height + (0.5 * offWall)],
    [offWall, height],
    [length + offWall, height],
    [length +  offWall + height, 0]
]);

translate([
    0,
    width - space,
    0
])
rotate([90, 0, 0])
linear_extrude(thickness)
polygon([
    [offWall, 0],
    [0, offWall],
    [0, height + (0.5 * offWall)],
    [0.5 * offWall, height + (0.5 * offWall)],
    [offWall, height],
    [length + offWall, height],
    [length +  offWall + height, 0]
]);

// Front Parts
color("Grey")
translate([
    offWall,
    width,
    0
])
rotate([90, 0, 0])
linear_extrude(thickness + space)
polygon([
    [length - 0.001, height],
    [length - 0.001, height + thickness],
    [length + height + thickness, 0],
    [length + height, 0],
]);

color("Blue")
translate([
    offWall,
    thickness + space,
    0
])
rotate([90, 0, 0])
linear_extrude(thickness + space)
polygon([
    [length - 0.001, height],
    [length - 0.001, height + thickness],
    [length + height + thickness, 0],
    [length + height, 0],
]);

/*XXX / Top slop
difference() {
translate([
    0,
    width,
    0
])
rotate([90, 0, 0])
linear_extrude(width)
polygon([
    [0, 2 * height],
    [offWall, height],
    [offWall, height + thickness],
    [0, 2 * height + thickness]
]);

// Top cutout
color("Red")
translate([
    -0.001,
    thickness + space + 0.001,
    0
])
cube([
    2 + 0.001,
    width - 2 * thickness - 2 * space - 0.002,
    2 * height + thickness
]);
};*/
    

// Top slop
color("Red")
translate([
    0,
    width,
    0
])
rotate([90, 0, 0])
linear_extrude(width)
polygon([
    [0.5*offWall, height + (0.5*offWall)],
    [offWall, height],
    [offWall, height + thickness],
    [0.5 * offWall, height + (0.5*offWall) + thickness]
]);

// Crowns
color("Pink")
translate([
  0, 0, height + (0.5*offWall) - 0.001
])
cube([
  0.5 * offWall,
  thickness + space,
  thickness + 0.001
]);

color("Brown")
translate([
  0,
  width - thickness - space,
  height + (0.5*offWall) - 0.001
])
cube([
  0.5 * offWall,
  thickness + space,
  thickness + 0.001
]);

// Tabs
color("Black")
translate([
  offWall,
  -bump,
  height
])
cube([
  bumpLength,
  bump + 0.001,
  thickness
]);

color("Black")
translate([
  offWall + length - bumpLength,
  -bump,
  height
])
cube([
  bumpLength,
  bump + 0.001,
  thickness
]);

color("Black")
translate([
  offWall,
  width,
  height
])
cube([
  bumpLength,
  bump + 0.001,
  thickness
]);

color("Black")
translate([
  offWall + length - bumpLength,
  width,
  height
])
cube([
  bumpLength,
  bump + 0.001,
  thickness
]);