# vista-lite-fly-stopper

3D model for the small plastic fly stoppers for the drains on
[VistaLite](https://vistalite.co.nz/) window frames.

## Model
![Model](images/model.png)

The [model](fly-stopper.scad) is an [OpenSCAD][] model, broken down into the
sides, flat section, front ramps and rear ramps. The model size can be
controlled with the following variables:
* `thickness` - thickness of plastic
* `width` - width of channel (window to sash)
* `height` - height of sides
* `length` - length of flat section of stopper
* `space` - space between outside of sides and side of flat section

## Original
![Original fly stopper](images/original.jpg)

![Original fly stopper in-place](images/original_inplace.jpg)

[OpenSCAD]: http://openscad.org/
